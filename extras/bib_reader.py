import csv
import sys
from pathlib import Path

from pinpoint.prepare import load_resource, save_resource

csv.field_size_limit(2147483647)


class BibReader(object):
    """
    Import datasets from Scopus and Web of Science and prepare it for analytics with PinPoint.
    """

    def __init__(self):
        self.affiliations = set()
        self.weighted_affiliations = list()
        self.publications = list()
        pass

    def save(self, file_path):
        """
        Export the processed dataset

        Args:
            file_path (str OR Path): File path to save
        """

        save_resource(data_dict={'affiliations': self.affiliations,
                                 'weighted_affiliations': self.weighted_affiliations,
                                 'publications': self.publications},
                      path=file_path)

    def _load_bib_reader(self, path):
        resources = load_resource(path)
        self.affiliations = set(resources['affiliations'])
        self.weighted_affiliations = resources['weighted_affiliations']
        self.publications = resources['publications']

    def load_file(self, file_path, source='auto'):
        """
        Import a given file.

        Args:
            file_path (str OR Path):
            source (str): 'auto', 'scopus' or 'wos'
        """

        file_path = Path(file_path)

        if source == 'auto':
            if file_path.suffix == '.csv':
                self._load_scopus(file_path)
            elif file_path.suffix == '.txt':
                self._load_wos(file_path)
            elif file_path.suffix == '.gzip':
                self._load_bib_reader(file_path)
            else:
                print(f"Not a expected extension ({file_path.suffix})", file=sys.stderr)
        elif source == 'scopus':
            self._load_scopus(file_path)
        elif source == 'wos':
            self._load_wos(file_path)
        else:
            print(f"Not a expected source type ({source})", file=sys.stderr)

        self.affiliations.discard('')
        self.affiliations.discard(None)

    def load_files(self, file_list, source='auto'):
        """
        Import a list of files.

        Args:
            file_list (list[str OR Path]): List of file paths
            source (str): 'auto', 'scopus' or 'wos'
        """

        for file_path in file_list:
            self.load_file(file_path, source)

    def load_folder(self, folder, source='auto'):
        """
        Import all files in a folder.

        Args:
            folder (str OR Path): Folder path
            source (str): 'auto', 'scopus' or 'wos'
        """

        folder = Path(folder)
        if folder.is_dir():
            for file_path in folder.iterdir():
                if file_path.name.startswith('_'):
                    continue
                else:
                    self.load_file(file_path, source)
        else:
            print(f"Error: Not a folder ({folder})", file=sys.stderr)

    def _load_scopus(self, file_path):
        with open(file_path, 'r', encoding='utf-8-sig') as f:
            reader = csv.DictReader(f)
            for row in reader:
                try:
                    publication = self._prepare_scopus_csv(row)
                except:
                    continue
                if publication:
                    self.publications.append(publication)
                    self.weighted_affiliations.append(publication['weighted_affiliation'])
                    self.affiliations.update(publication['weighted_affiliation'].keys())

    def _load_wos(self, csv_file):
        with open(csv_file, 'rtU', encoding='utf-8') as f:
            reader = csv.DictReader(f, dialect=csv.excel_tab)
            for row in reader:
                try:
                    publication = self._prepare_wos_csv(row)
                except:
                    continue
                if publication:
                    self.publications.append(publication)
                    self.weighted_affiliations.append(publication['weighted_affiliation'])
                    self.affiliations.update(publication['weighted_affiliation'].keys())

    @staticmethod
    def _prepare_scopus_csv(raw_data):
        publication = dict()
        try:
            year = int(raw_data['Year'].strip())
        except ValueError:
            year = 0
        if year >= 1850:
            publication['year'] = year
        else:
            publication['year'] = None

        publication['doi'] = raw_data['DOI'].strip()
        try:
            citations = int(raw_data['Cited by'])
        except ValueError:
            citations = 0
        if citations >= 0:
            publication['citations'] = citations
        else:
            publication['citations'] = None

        weighted_affiliation = dict()
        for affiliation in raw_data['Affiliations'].split(';'):
            weighted_affiliation[affiliation.strip()] = 0

        for author in raw_data['Authors with affiliations'].split(';'):
            for affiliation in weighted_affiliation:
                if affiliation in author:
                    weighted_affiliation[affiliation] += 1

        publication['weighted_affiliation'] = weighted_affiliation
        publication['raw'] = raw_data
        return publication

    @staticmethod
    def _prepare_wos_csv(raw_data):

        publication = dict()
        publication['doi'] = raw_data['DI'].strip()
        try:
            year = int(raw_data['PY'].strip())
        except ValueError:
            year = 0
        if year >= 1850:
            publication['year'] = year
        else:
            publication['year'] = None

        try:
            citations = int(raw_data['TC'])
        except ValueError:
            citations = 0
        if citations >= 0:
            publication['citations'] = citations
        else:
            publication['citations'] = None

        affiliation_raw = raw_data['C1']

        weighted_affiliation = dict()
        author_affiliation = dict()
        for entry in affiliation_raw.split('['):
            if entry.strip():
                authors_raw = entry.partition(']')[0].split('; ')
                affiliation = entry.partition(']')[2].strip()
                if affiliation.endswith(';'):
                    affiliation = affiliation[:-1]
                for author in authors_raw:
                    if author.strip() in author_affiliation:
                        author_affiliation[author.strip()].append(affiliation)
                    else:
                        author_affiliation[author.strip()] = [affiliation]
        for author in author_affiliation:
            for affiliation in author_affiliation[author]:
                if affiliation in weighted_affiliation:
                    weighted_affiliation[affiliation] += 1/len(author_affiliation[author])
                else:
                    weighted_affiliation[affiliation] = 1 / len(author_affiliation[author])

        publication['weighted_affiliation'] = weighted_affiliation
        publication['raw'] = raw_data
        return publication
