import math
import random
import time
from pathlib import Path

from geopy.distance import distance
from geopy.geocoders import Bing, GoogleV3

from pinpoint import Locator
from pinpoint.prepare import load_resource, save_resource


class Benchmark(object):
    """
    Benchmark PinPoint and compare to online APIs

    Usage:
        from example import Benchmark

        # give here your bing_key and/or google_key to use the online APIs
        bm = Benchmark(bing_key='foobar123', google_key='foobar123')
        bm.speed()
    """

    def __init__(self, bing_key=None, google_key=None, cache=True, test_affiliations=None, server=False):
        """
          Prepare Benchmark

          Args:
              bing_key (str): Bing Maps API key
              google_key (str): Google Maps API key
              cache (bool): Use internal cache when access online APIs
              test_affiliations (list[str]): give an own list of affiliation strings as test cases
              server (bool): use redis subsystem
        """
        print('Init PinPoint Benchmark')
        self.base_dir = Path(__file__).absolute().parent
        self.cache_dir = self.base_dir / 'data/cache'
        self.cache = cache
        if self.cache:
            self.cache_dir.mkdir(exist_ok=True)
            self.cache_file = self.cache_dir / 'requests.gzip'
            if self.cache_file.is_file():
                self.cache_data = load_resource(self.cache_file)
            else:
                self.cache_data = dict(gmap=dict(), bing=dict())
        self.bing_key = bing_key
        self.google_key = google_key

        if self.google_key:
            self.gmap_geolocator = GoogleV3(api_key=self.google_key, timeout=5)
        if self.bing_key:
            self.bing_geolocator = Bing(api_key=self.bing_key, timeout=5)

        print('  Load test resources (this may takes a while)')
        self.loc = Locator(server=server)
        if test_affiliations is None:
            default_affiliation_test = self.base_dir / 'data/nano_2017.gzip'
            print(f'  Load affiliation test cases ({default_affiliation_test}).\n')
            self.test_affiliations = load_resource(default_affiliation_test)

    def _save_cache(self):
        if self.cache:
            save_resource(self.cache_data, self.cache_file)

    def gmap_geocode(self, string):
        """
            Args:
                string (str): String to be geocoded

            Returns dict: Location (keys: 'address', 'latitude', 'longitude')

        """
        if self.cache:
            if string in self.cache_data['gmap']:
                location = self.cache_data['gmap'][string]
            else:
                location = list(self.gmap_geolocator.geocode(string, exactly_one=True))
                self.cache_data['gmap'][string] = location
        else:
            location = list(self.gmap_geolocator.geocode(string, exactly_one=True))

        return dict(address=location[0], latitude=location[1][0], longitude=location[1][1])

    def bing_geocode(self, string):
        """
            Args:
                string (str): String to be geocoded

            Returns dict: Location (keys: 'address', 'latitude', 'longitude')

        """
        if self.cache:
            if string in self.cache_data['bing']:
                location = self.cache_data['bing'][string]
            else:
                location = list(self.bing_geolocator.geocode(string, exactly_one=True))
                self.cache_data['bing'][string] = location
        else:
            location = list(self.bing_geolocator.geocode(string, exactly_one=True))

        return dict(address=location[0], latitude=location[1][0], longitude=location[1][1])

    def speed(self, sample_number=100000):
        """
            Measure how many strings can be geocoded per second with PinPoints find() function.

            Args:
                sample_number (int): number of samples to take

        """
        print(f'Benchmark Locator.find() speed on {sample_number} samples')
        if sample_number >= len(self.test_affiliations):
            factor = math.ceil(sample_number/len(self.test_affiliations))
            test_cases = random.sample(self.test_affiliations*factor, sample_number)
        else:
            test_cases = random.sample(self.test_affiliations, sample_number)
        start = time.time()
        for test_string in test_cases:
            self.loc.find(test_string)
        stop = time.time()
        speed = int(len(test_cases)/(stop-start))
        print(f'  Analysed {sample_number} strings in {stop-start:.2f} seconds - {speed} strings per seconds\n')

    def hits(self, details=False):
        """
            Measure the ratio of strings that could be geocoded.

            Args:
                details (bool): if True print additional country information

        """
        print('Benchmark find() hit ratio')
        no_country = []
        no_city = []
        for test_string in self.test_affiliations:
            country, region, city = self.loc.find(test_string)
            if country is None:
                no_country.append(test_string)
                no_city.append((test_string, None))
                continue
            if city is None:
                no_city.append((test_string, country))

        print(f'  {len(self.test_affiliations)} strings tested')
        print(f'  {len(no_country)} strings without country ({len(no_country)*100/len(self.test_affiliations):.2}%).')
        print(f'  {len(no_city)} strings without city ({len(no_city)*100/len(self.test_affiliations):.2}%).\n')
        if details:
            country_count = {}
            for test_string, country in no_city:
                if country:
                    try:
                        country_count[country['name']] += 1
                    except KeyError:
                        country_count[country['name']] = 1

            for country, count in sorted(country_count.items(), key=lambda x: x[1]):
                print(f'    {count} strings from {country}.')

    def sample(self, sample_number=20):
        """
             Samples results from the PinPoint find() function.

             Args:
                 sample_number (int): number of samples to take

         """
        print(f'Sample {sample_number} results from find()')
        if sample_number >= len(self.test_affiliations):
            test_cases = self.test_affiliations
        else:
            test_cases = random.sample(self.test_affiliations, sample_number)
        for test_string in test_cases:
            country, region, city = self.loc.find(test_string)

            print(f'  {test_string}')
            if country:
                print(f'    Continent: {country["continent"]}\n    Country: {country["name"]} ({country["a2"]})')
            else:
                print('    NO COUNTRY')
            if region:
                print(f'    Region: {region["name"]} ({region["region_code"]})')
            if city:
                print(f'    City: {city["name"]} ({city["latitude"]}, {city["longitude"]})')
            else:
                print('    NO CITY')
            print()

    def no_city_sample(self, sample_number=20):
        """
             Show results from PinPoint find() function that did not return a city.

             Args:
                 sample_number (int): number of samples to take

        """
        print(f'Sample {sample_number} results from find() that did not return a city')
        no_city = []
        for test_string in self.test_affiliations:
            country, region, city = self.loc.find(test_string)
            if country is None:
                continue
            if city is None:
                no_city.append((test_string, country))

        print(f'  {len(no_city)} possible results\n')
        if sample_number >= len(no_city):
            test_cases = no_city
        else:
            test_cases = random.sample(no_city, sample_number)
        for result in test_cases:
            test_string, country = result

            print(f'  {test_string}')
            print(f'    Continent: {country["continent"]}')
            print(f'    Country: {country["name"]} ({country["a2"]})\n')

    def no_city_by_country(self, country_code):
        """
             Show all results from PinPoint find() function that did not return a city for a specific country.

             Args:
                 country_code (str): ISO 3166-1 alpha-2 country code ('US', 'DE', 'GB', ...)

        """
        print(f'All results from Locator.find() that did not return a city for {country_code}')
        no_city = []
        for test_string in self.test_affiliations:
            country, region, city = self.loc.find(test_string)
            if country is None:
                continue
            if city is None:
                no_city.append((test_string, country))

        print(f'  {len(no_city)} possible results\n')

        for result in no_city:
            test_string, country = result
            if country['a2'] == country_code.upper():
                print(f'  {test_string}')

    def no_country_sample(self, sample_number=20):
        """
             Show a sample of results from PinPoint find() that did not return a country.

             Args:
                 sample_number (int): number of samples to take

        """
        print(f'Sample {sample_number} results from find() that did not return a country')
        no_country = []
        for test_string in self.test_affiliations:
            country, region, city = self.loc.find(test_string)
            if country is None:
                no_country.append(test_string)
        print(f'  {len(no_country)} possible results\n')

        if sample_number >= len(no_country):
            test_cases = no_country
        else:
            test_cases = random.sample(no_country, sample_number)
        for test_string in test_cases:
            print(f'  {test_string}')

    def _speed_apis_run(self, coder, sample_number):
        if sample_number >= len(self.test_affiliations):
            test_cases = self.test_affiliations
        else:
            test_cases = random.sample(self.test_affiliations, sample_number)
        start = time.time()
        cache_status = self.cache
        self.cache = False
        for test_string in test_cases:
            coder(test_string)
        stop = time.time()
        self.cache = cache_status
        speed = sample_number / (stop - start)
        print(f'  Analysed {sample_number} strings in {stop-start:.2f} seconds - {speed:.2f} strings per seconds\n')

    def speed_api(self, sample_number=5, service=None):
        """
            Measure how many strings can be geocoded per second with common online APIs.

            Args:
                sample_number (int): number of samples to take
                service (str): specify an API ('bing', 'google') if None all are tested

        """
        if service not in ['bing', 'google', None]:
            print(f'The given service {service} is not valid.')
            return
        if service == 'bing' or service is None:
            print(f'Benchmark Bing Maps API speed on {sample_number} samples')
            if not self.bing_key:
                print('No Bing API key')
                return
            coder = self.bing_geocode
            self._speed_apis_run(coder, sample_number)
        if service == 'google' or service is None:
            print(f'Benchmark Google Maps API speed on {sample_number} samples')
            if not self.google_key:
                print('No Google API key')
                return
            coder = self.gmap_geocode
            self._speed_apis_run(coder, sample_number)

    def compare_address(self, sample_number=5, service=None):
        """
            Compare the PinPoint geocode results with common online APIs.

            Args:
                sample_number (int): number of samples to take
                service (str): specify an API ('bing', 'google'), if None all are tested
        """
        if service == 'bing':
            print(f'Sample {sample_number} locations and compare the result from find() with Bing Maps API')
        elif service == 'google':
            print(f'Sample {sample_number} locations and compare the result from find() with Google Maps API')
        elif service is None:
            print(f'Sample {sample_number} locations and compare the result from find() with '
                  f'Bing Maps API and Google Maps API')
        else:
            print(f'The given service {service} is not valid.')
            return
        for test_string in random.sample(self.test_affiliations, sample_number):
            print('\n', test_string)
            country, region, city = self.loc.find(test_string)
            if city:
                print(f" PinPoint: {city['name']}, {country['name']}")
            else:
                print(f" PinPoint: NO CITY")

            if self.bing_key and (service is None or service == "bing"):
                location = self.bing_geocode(test_string)
                if location:
                    print(f"     Bing: {location.address}")
                else:
                    print(f"     Bing: NO ADDRESS")

            if self.google_key and (service is None or service == "google"):

                location = self.gmap_geocode(test_string)
                if location:
                    print(f"   Google: {location.address}")
                else:
                    print(f"   Google: NO ADDRESS")

        self._save_cache()

    def compare_distance(self, sample_number=5, service=None):
        """
            Show the distance between the PinPoint geocode results to common online APIs.

            Args:
                sample_number (int): number of samples to take
                service (str): specify an API ('bing', 'google'), if None all are tested
        """
        if service == 'bing':
            print(f'Sample {sample_number} locations and show the distance between find() and Bing Maps API')
        elif service == 'google':
            print(f'Sample {sample_number} locations and show the distance between find() and Google Maps API')
        elif service is None:
            print(f'Sample {sample_number} locations and show the distance between find() with '
                  f'Bing Maps API and Google Maps API')
        else:
            print(f'The given service {service} is not valid.')
            return
        for test_string in random.sample(self.test_affiliations, sample_number):
            print('\n', test_string)
            country, region, city = self.loc.find(test_string)
            if city:
                print(f" PinPoint: {city['name']}, {country['name']}")
                print(f"           {city['latitude']}, {city['longitude']}")
            else:
                print(f" PinPoint: NO CITY")
                continue

            if self.bing_key and (service is None or service == "bing"):
                location = self.bing_geocode(test_string)
                if location:
                    print(f"     Bing: {location['address']}")
                    print(f"           {location['latitude']}, {location['longitude']}")
                    bing_distance = distance((city['latitude'], city['longitude']),
                                             (location['latitude'], location['longitude'])).km
                    print(f"           {round(bing_distance,2)} km")
                else:
                    print(f"     Bing: NO ADDRESS")

            if self.google_key and (service is None or service == "google"):

                location = self.gmap_geocode(test_string)
                if location:
                    print(f"   Google: {location['address']}")
                    print(f"           {location['latitude']}, {location['longitude']}")
                    google_distance = distance((city['latitude'], city['longitude']),
                                               (location['latitude'], location['longitude'])).km
                    print(f"           {round(google_distance,2)} km")
                else:
                    print(f"   Google: NO ADDRESS")

        self._save_cache()
