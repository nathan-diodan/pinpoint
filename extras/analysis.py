import csv
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from pinpoint import Locator
from pinpoint.prepare import save_resource, load_resource
from .bib_reader import BibReader

csv.field_size_limit(2147483647)


class DataSetAnalysis(object):
    """
    Analyse documents with PinPoint

    Usage:
        from extras import DataSetAnalysis
        dsa = DataSetAnalysis()
        dsa.load_data('scopus.csv', kind='scopus')
        dsa.plot_fingerprint()
    """
    def __init__(self):
        self.loc = Locator()
        self.paper_set = []
        self.distance = []
        pass

    def load_data(self, data_input, kind):
        """
        Load previously saved affiliation data or
        import from 'Scopus' or 'Web of Science' datasets.

        Args:
            data_input (str OR Path): File or folder path
            kind (str): 'raw' to import files that were saved from this class or 'scopus' / 'wos' for external imports
        """

        br = BibReader()
        if isinstance(data_input, str):
            input_paths = [Path(data_input)]
        else:
            input_paths = []
            for input_entry in data_input:
                input_paths.append(Path(input_entry))
        input_file_paths = []
        for input_path in input_paths:
            if input_path.is_dir():
                for folder_file_path in input_path.iterdir():
                    if folder_file_path.is_file():
                        input_file_paths.append(folder_file_path)
            elif input_path.is_file():
                input_file_paths.append(input_path)

        if kind.lower() in ['scopus', 'wos']:
            print('Load files:')
            for file_path in input_file_paths:
                if file_path.suffix in ['.csv', '.txt']:
                    br.load_file(file_path=file_path, source=kind.lower())
            local_paper_set = br.publications

            print(f'Process {len(local_paper_set)} documents:')
            self.paper_set.extend(local_paper_set)
            status = max(1, int(len(local_paper_set)/100))
            for n, paper in enumerate(local_paper_set):

                location_data = self.loc.calculate_str(paper['weighted_affiliation'])
                paper['cooperation_distance'] = location_data[0]
                paper['apparent_location'] = location_data[1]
                if location_data[0] is not None:
                    self.distance.append(location_data[0])
                if (n+1) % status == 0:
                    percent = (n+1)/len(local_paper_set)*100
                    print(f'\t{percent:.1f}% done')

        elif kind.lower() == 'raw':
            for file_path in input_file_paths:
                if file_path.suffix == '.gzip':
                    self.load_raw(file_path)

    def load_raw(self, file_path):
        """
        Load file created with save_raw function.

        Args:
            file_path: File path
        """

        raw_data = load_resource(file_path)
        self.paper_set.extend(raw_data['paper_set'])
        self.distance.extend(raw_data['distances'])

    def clear(self):
        """
        Clear the internal memory. Useful before loading a new dataset.
        """

        self.paper_set = []
        self.distance = []

    def save_raw(self, file_path):
        """
        Save the imported data to speed up future analysis work.
        File can be loaded with load_data(file_path, kind='raw')

        Args:
            file_path: Save file path
        """

        raw_data = dict(paper_set=self.paper_set,
                        distances=self.distance)
        save_resource(raw_data, file_path)

    def save_csv(self, file_path):
        """
        Export all documents into a CSV file.
        Following information are included:
            doi, citations, year, cooperation_distance,
            apparent_location, weighted_affiliation

        Args:
            file_path: CSV file path
        """

        file_path = Path(file_path)
        with file_path.open('w', newline='') as csv_file:
            fieldnames = ['doi', 'citations', 'year', 'cooperation_distance',
                          'apparent_location', 'weighted_affiliation']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            for paper in self.paper_set:
                writer.writerow(paper)

    def fingerprint(self):
        """
        Generating a cooperation fingerprint for the loaded dataset.

        Returns:
            dict: 'fraction', 'distances'

        """
        distances = np.sort(np.array(self.distance))
        if len(distances) > 1000:
            fraction_select = np.linspace(0, len(distances)-1, 1000, endpoint=True, dtype='int')
            distances = distances[fraction_select]
        fraction = np.linspace(0, 100, len(distances), endpoint=True)
        return {'fraction': fraction, 'distances': distances}

    def plot_fingerprint(self, file_path=None):
        """
        Plot dataset fingerprint.

        Args:
            file_path (str): File path to save the figure

        Returns:

        """
        fp = self.fingerprint()
        plt.figure(figsize=(8, 6))
        plt.plot(fp['fraction'], fp['distances'], color='blue', linewidth=2.5, linestyle="-")
        plt.xlim((100-fp['fraction'][np.sum(fp['distances'] > 0)], 100))
        plt.ylim((0, 10000))
        plt.ylabel('Cooperation distance in km')
        plt.xlabel('Document fraction in %')
        plt.title('Cooperation distance fingerprint')
        if file_path is None:
            plt.show()
        else:
            plt.savefig(file_path, bbox_inches='tight')

    def split(self, split=500):
        """
        The fraction of documents above the split.

        Args:
            split (int): Cooperation distance split in kilometer

        Returns:
            float: Fraction in percent
        """
        distances = np.array(self.distance)
        return round(len(distances[distances > split])/len(distances) * 100, 2)

    def year_split(self, split=500):
        """
        Returns the fraction of documents above the split over the years.

        Args:
            split (int): Cooperation distance split in kilometer

        Returns:
            dict: 'year', 'ratio'

        """
        data_collection = dict()
        for paper in self.paper_set:
            try:
                year = int(paper['year'])
                try:
                    if paper['cooperation_distance'] <= split:
                        data_collection[year][0] += 1
                    else:
                        data_collection[year][1] += 1
                except KeyError:
                    if paper['cooperation_distance'] <= split:
                        data_collection[year] = [1, 0]
                    else:
                        data_collection[year] = [0, 1]
            except:
                pass

        ratio = np.array(list(map(lambda y: 100 * y[1] / sum(y), data_collection.values())))
        year = np.array(list(data_collection.keys()))
        order = np.argsort(year)
        year = year[order]
        ratio = ratio[order]
        return {'year': year, 'ratio': ratio}

    def plot_year_split(self, file_path=None, split=500):
        """
        Plot the fraction of documents above the split over the years.

        Args:
            file_path (str): File path to save the figure
            split (int): Cooperation distance split in kilometer
        """
        ys = self.year_split(split)
        plt.bar(ys['year'], ys['ratio'], align='center', alpha=0.5)
        plt.xlabel('Year')
        plt.ylabel('Document fraction in %')
        plt.title(f'Cooperation distance split > {split} km')
        if file_path is None:
            plt.show()
        else:
            plt.savefig(file_path, bbox_inches='tight')

    def quality_split(self, split=500):
        """
        Return average citations of documents above and below/at the split.

        Args:
            split (int): Cooperation distance split in kilometer

        Returns:
            dict: 'average_citations', 'average_citations_above', 'average_citations_below'

        """
        result = {}
        citation_over = []
        citation_under = []
        for paper in self.paper_set:
            if paper['citations']:
                cite = paper['citations']
            else:
                cite = 0
            if paper['cooperation_distance']:
                if paper['cooperation_distance'] <= split:
                    citation_under.append(cite)
                else:
                    citation_over.append(cite)
            else:
                citation_under.append(cite)
        result['average_citations'] = (sum(citation_over) + sum(citation_under)) / len(self.paper_set)
        result['average_citations_above'] = sum(citation_over)/len(citation_over)
        result['average_citations_below'] = sum(citation_under)/len(citation_under)
        return result

    def quality_range(self, bins=10):
        """
        Return average citations of documents in equally sized bins.
        (All documents with a cooperation distance of zero are combined in the first bin)

        Args:
            bins (int): Number of bins to create

        Returns:
            dict: 'age_citations', 'bin_edges', 'bin_width', 'bin_count', 'over_all_average'

        """
        distance = np.sort(np.array(self.distance))
        start = min(np.argwhere(distance > 0))
        fraction_select = distance[np.linspace(start, len(distance) - 1, bins, endpoint=True, dtype='int')]
        distance = []
        citation = []
        for paper in self.paper_set:
            if paper['citations']:
                citation.append(paper['citations'])
            else:
                citation.append(0)
            if paper['cooperation_distance']:
                distance.append(paper['cooperation_distance'])
            else:
                distance.append(0)
        distance = np.array(distance)
        citation = np.array(citation)
        num = [len(citation[distance == 0])]
        average_citations = [np.average(citation[distance == 0])]

        for i in range(len(fraction_select)-1):
            average_citations.append(np.average(citation[np.logical_and(distance >= fraction_select[i],
                                                                        distance < fraction_select[i+1])]))
            num.append(len(citation[np.logical_and(distance >= fraction_select[i],
                                                   distance < fraction_select[i+1])]))
        result = dict(
            average_citations=average_citations,
            bin_edges=[0] + list(fraction_select)[:-1],
            bin_width=[fraction_select[0]] + list(np.diff(fraction_select)),
            bin_count=num,
            over_all_average=np.average(citation)
        )
        return result

    def plot_quality_range(self, file_path=None, bins=10):
        """
        Plot the average citations of documents in equally sized bins

        Args:
            file_path (str): File path to save the figure
            bins (int): Number of bins to create
        """
        qr = self.quality_range(bins=bins)
        plt.bar(qr['bin_edges'][1:], qr['average_citations'][1:], align='edge', width=qr['bin_width'][1:],
                edgecolor='black')
        plt.xlabel('Cooperation distance in km')
        plt.ylabel('Average citations (green: overall, red: cd=0)')
        plt.title(f'Citations over distance'),
        plt.xscale("log")
        plt.axhline(qr['average_citations'][0], color='red')
        plt.axhline(qr['over_all_average'], color='green')
        if file_path is None:
            plt.show()
        else:
            plt.savefig(file_path, bbox_inches='tight')
