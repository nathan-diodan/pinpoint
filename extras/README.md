# PinPoint Extras

PinPoint is a fast geo toolkit for academic affiliation strings.
In this folder are different examples and tests for the PinPoint module.

## Benchmark

In this package, multiple tests are included to benchmark PinPoint.

### Usage

To use all tests API keys for Google and Bing Maps need to be passed on
initialization. The default test case is a list of unique affiliation strings
found in the field of Nanotechnology in 2017.

```python
from extras import Benchmark
bm = Benchmark(bing_key='foobar123', google_key='foobar123')
```

The following tests can than be used:
```python
bm.speed()
bm.hits()
bm.sample()
bm.no_city_sample()
bm.no_city_by_country()
bm.no_country_sample()
bm.speed_api()
bm.compare_address()
bm.compare_distance()
```

## DataSetAnalysis

This package contains examples on how to analyze datasets with PinPoint.

### Prepare

After initializing the module, you can import data from Scopus or Web of
Science. You can either load a single file, a list of files or a
complete folder.

```python
from extras import DataSetAnalysis
dsa = DataSetAnalysis()
dsa.load_data('scopus.csv', kind='scopus')
```

To export from Web of Science:

* Search [Web of Science](http://apps.webofknowledge.com/)
* Over the result list click on ⋁ next to *Save to EndNote online*
* Select *Save to Other File Formats*
* The *Send to File* menu should open
* In Number of Records choose the second option and type in from 1 to number of results as displayed on the left
* In Record Content select Full Record
* In File Format select Tab-delimited (Mac, UTF-8)
* Click Send

To export from Scopus:

* Search [Scopus](https://www.scopus.com/search/)
* Over the result list click on ⋁ next to All or Page
* Click Select all
* Click Export
* The Export document settings menu should open
* In Select your method of export choose CSV (Excel)
* In Customize export select Citation information and Bibliographical information
* Click Export

If the dataset is extensive, it is useful to save the generated data and
load it directly later.

```python
dsa.save_raw('my_save.gzip')
dsa.load_data('my_save.gzip', kind='raw')
```

### Examples

#### Fingerprint

A fingerprint shows how the cooperation distance is distributed
over the dataset. You can either generate the results or directly plot it.
```python
result = dsa.fingerprint()
dsa.plot_fingerprint()
```

#### Split

Splitting the dataset at a specific cooperation distance can be useful to
compare to the [Snowball](https://www.snowballmetrics.com/metrics/)
Collaboration metrics for example. The default split is at 500 km. You
can also generate the split over the years.

```python
result_split = dsa.split()
result_year_split = dsa.year_split(split=1000)
dsa.plot_year_split()
```

#### Quality

Another interesting question if the quality of scientific publications is
enhanced by large-scale cooperation. In the basic `quality_split()`
function the average citation can be compared with the one above to the
one below/at the split.

```python
result_split = dsa.quality_split()
```

In the second variant, the documents are separated in roughly equal-sized
groups, to show the relationship between cooperation distance and average
citations. (The group for documents with a cooperation distance of zero
is often much larger than the other groups). The default number of bins is ten.

```python
result_quality_range = dsa.quality_range()
dsa.plot_quality_range()
```


#### Export

To work with other tools, you can export the document data as CSV file
including DOI, citations, year, cooperation distance, the apparent location,
and weighted affiliations.

```python
dsa.save_csv('my_save.csv')
```