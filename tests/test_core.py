import unittest

from pinpoint import Locator


class TestLocator(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.loc = Locator(refresh=True)

    @classmethod
    def tearDownClass(cls):
        del cls.loc

    def test_find_normal(self):
        test_string = "Dresden Center for Computational Material Science, Technische Universität Dresden, Dresden, Germany"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '2921044')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '2935022')

        test_string = "Department of Chemical and Biomolecular Engineering, Rice University, Houston, TX, United States"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '6252001')
        self.assertEqual(region['geonameid'], '4736286')
        self.assertEqual(city['geonameid'], '4699066')

        test_string = "Nanoscience and Nanotechnology Center, Institute of Scientific and Industrial Research (ISIR), Osaka University, 8-1 Mihogaoka, Ibaraki, Osaka, Japan"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '1861060')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '1853909')

        test_string = "Centro/Departamento de Física da Universidade do Minho, Campus de Gualtar, 4710-057 Braga, Portugal"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '2264397')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '2742032')

    def test_find_unicode_replace(self):
        test_string = "Department of Chemical and Biomolecular Engineering, Rice University, Houstön, TX, United States"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '6252001')
        self.assertEqual(region['geonameid'], '4736286')
        self.assertEqual(city['geonameid'], '4699066')

        test_string = "Centro/Departamento de Física da Universidade do Minho, Campus de Gualtar, 4710-057 Bräga, Portugal"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '2264397')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '2742032')

    def test_find_order(self):
        test_string = "London, United Kingdom, Royal Academy"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '2635167')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '2643743')

    def test_find_unicode(self):
        test_string = "ロンドン, संयुक्त राजशाही"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '2635167')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '2643743')

    def test_find_missing_region(self):
        test_string = "Department of Chemical and Biomolecular Engineering, Rice University, Houston, United States"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '6252001')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '4699066')

    def test_find_zip_code(self):
        test_string = "93310, France"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '3017382')
        self.assertEqual(region, None)
        self.assertEqual(city['geonameid'], '3002499')

        test_string = "77006, TX, USA"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '6252001')
        self.assertEqual(region['geonameid'], '4736286')
        self.assertEqual(city['geonameid'], '4699066')

    def test_find_missing(self):
        test_string = "TX, USA"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '6252001')
        self.assertEqual(region['geonameid'], '4736286')
        self.assertEqual(city, None)

        test_string = "Fantasy, France"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country['geonameid'], '3017382')
        self.assertEqual(region, None)
        self.assertEqual(city, None)

        test_string = ""
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country, None)
        self.assertEqual(region, None)
        self.assertEqual(city, None)

        test_string = "GER"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country, None)
        self.assertEqual(region, None)
        self.assertEqual(city, None)

        test_string = "London"
        country, region, city = self.loc.find(test_string)
        self.assertEqual(country, None)
        self.assertEqual(region, None)
        self.assertEqual(city, None)

    def test_weighted_affiliations(self):
        weighted_affiliations = {
            "Dresden Center for Computational Material Science, Technische Universität Dresden, Dresden, Germany": 2,
            "Department of Chemical and Biomolecular Engineering, Rice University, Houston, TX, United States": 1,
            "Nanoscience and Nanotechnology Center, Institute of Scientific and Industrial Research (ISIR), Osaka University, 8-1 Mihogaoka, Ibaraki, Osaka, Japan": 0.5,
            "Centro/Departamento de Física da Universidade do Minho, Campus de Gualtar, 4710-057 Braga, Portugal": 0.5,
        }

        cooperation_distance, apparent_location = self.loc.calculate_str(weighted_affiliations)
        self.assertEqual(cooperation_distance, 8382)
        self.assertEqual(round(apparent_location[0], 3), 64.692)
        self.assertEqual(round(apparent_location[1], 3), -15.274)

        weighted_affiliations = {
            "Dresden Center for Computational Material Science, Technische Universität Dresden, Dresden, Germany": 2,
            "Department of Chemical and Biomolecular Engineering, Rice University, Houston, TX, United States": 1,
            "Nanoscience and Nanotechnology Center, Institute of Scientific and Industrial Research (ISIR), Osaka University, 8-1 Mihogaoka, Ibaraki, Osaka, Japan": 0.5,
            "Centro/Departamento de Física da Universidade do Minho, Campus de Gualtar, 4710-057 Braga, Portugal": 0.5,
            "Fantasy, France": 0.5,
        }

        cooperation_distance, apparent_location = self.loc.calculate_str(weighted_affiliations)
        self.assertEqual(cooperation_distance, 8382)
        self.assertEqual(round(apparent_location[0], 3), 64.692)
        self.assertEqual(round(apparent_location[1], 3), -15.274)

        cooperation_distance, apparent_location = self.loc.calculate_str(dict())
        self.assertEqual(cooperation_distance, None)
        self.assertEqual(apparent_location, None)

    def test_weighted_coordinates(self):
        weighted_coordinate = (
            (2, (51.05089, 13.73832)),
            (1, (29.76328, -95.36327)),
            (0.5, (34.69374, 135.50218)),
            (0.5, (41.55032, -8.42005)),
        )

        cooperation_distance, apparent_location = self.loc.calculate_coordinates(weighted_coordinate)
        self.assertEqual(cooperation_distance, 8382)
        self.assertEqual(round(apparent_location[0], 3), 64.692)
        self.assertEqual(round(apparent_location[1], 3), -15.274)

        weighted_coordinate = (
            (0, (51.05089, 13.73832)),
            (0, (29.76328, -95.36327))
        )

        cooperation_distance, apparent_location = self.loc.calculate_coordinates(weighted_coordinate)
        self.assertEqual(cooperation_distance, None)
        self.assertEqual(apparent_location, None)

        cooperation_distance, apparent_location = self.loc.calculate_coordinates(None)
        self.assertEqual(cooperation_distance, None)
        self.assertEqual(apparent_location, None)


class TestLocatorServer(TestLocator):

    @classmethod
    def setUpClass(cls):
        cls.loc = Locator(server=True, refresh_server=True)


if __name__ == '__main__':
    unittest.main()
