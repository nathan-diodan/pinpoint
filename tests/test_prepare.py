import unittest
import tempfile
from pathlib import Path

from pinpoint.prepare import load_resource
from pinpoint.prepare import save_resource


class TestPrepare(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.folder = Path(tempfile.mkdtemp(prefix='pinpoint'))

    @classmethod
    def tearDownClass(cls):
        cls.folder.rmdir()
        del cls.folder

    def test_save_load(self):
        data_to_save = dict(
            treasure="The paper had been sealed in several places with a thimble by way of seal; "
                     "the very thimble, perhaps, that I had found in the captain's pocket. "
                     "The doctor opened the seals with great care, and there fell out the map of an island, "
                     "with latitude and longitude, soundings, names of hills and bays and inlets, "
                     "and every particular that would be needed to bring a ship to a safe anchorage upon its shores. "
                     "It was about nine miles long and five across, shaped, you might say, like a fat dragon standing up, "
                     "and had two fine land-locked harbours, and a hill in the centre part marked 'The Spy-glass.' "
                     "There were several additions of a later date, but above all, "
                     "three crosses of red ink--two on the north part of the island, "
                     "one in the southwest--and beside this last, in the same red ink, and in a small, "
                     "neat hand, very different from the captain's tottery characters, these words: 'Bulk of treasure here.'",
            monte="He glanced around, in order to seize on something on which the"
                  "conversation might turn, and seemed to fall easily on a topic. He saw"
                  "the map which Monte Cristo had been examining when he entered, and said:"
                  "\n"
                  "'You seem geographically engaged, sir? It is a rich study for you, who,"
                  "as I learn, have seen as many lands as are delineated on this map.'"
                  "\n"
                  "'Yes, sir,' replied the count; 'I have sought to make of the human race,"
                  "taken in the mass, what you practice every day on individuals—a"
                  "physiological study. I have believed it was much easier to descend from"
                  "the whole to a part than to ascend from a part to the whole. It is an"
                  "algebraic axiom, which makes us proceed from a known to an unknown"
                  "quantity, and not from an unknown to a known; but sit down, sir, I beg"
                  "of you.'",
            britannica="The shortest distance between two places onthe surface of a globe is represented by "
                       "the arc of a great circle. Ifthe two places are upon the same meridian or upon the equator "
                       "the exactdistance separating them is to be found by reference to a table givingthe lengths "
                       "of arcs of a meridian and of the equator. In all other casesrecourse must be had to a map, "
                       "a globe or mathematical formula.Measurements made on a topographical map yield the most "
                       "satisfactoryresults. Even a general map may be trusted, as long as we keep withinten "
                       "degrees of its centre. In the case of more considerable distances,however, a globe of "
                       "suitable size should be consulted, or --and thisseems preferable-- they should be calculated "
                       "by the rules of sphericaltrigonometry. The problem then resolves itself in the "
                       "solution of aspherical triangle."
        )
        save_path = self.folder / 'test_dict'
        save_resource(data_to_save, save_path)
        self.assertTrue(save_path.with_suffix('.gzip').is_file())
        loaded_data = load_resource(save_path.with_suffix('.gzip'))
        self.assertEqual(data_to_save, loaded_data)
        save_path.with_suffix('.gzip').unlink()


if __name__ == '__main__':
    unittest.main()