"""
A fast geo toolkit for academic affiliation strings

Usage:
    the core functions are bundled in Locator
    from pinpoint import Locator
    loc = Locator()
"""
from .core import Locator
